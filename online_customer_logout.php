<?php
session_start();
include('config.php');
if (!$conn) {
	die("Connection failed: " . mysqli_connect_error());
}
else
{
	session_unset();
    session_destroy();
    session_write_close();
}

?>

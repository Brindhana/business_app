var app = angular.module("myApp", ["ngRoute","ngSanitize"]); //ngSanitize -used to print html design
//For get template title and template url. example if we call home_page, the title will be a "Home Title"
app.run(['$location', '$rootScope', function($location, $rootScope) {
    $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
        if (current.hasOwnProperty('$$route')) {
            $rootScope.title = current.$$route.title;
            $rootScope.templateUrl = current.$$route.templateUrl;
        }
    });
}]);

app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        title: 'Index page title',                  
        templateUrl : "partials/main.html",
        controller : "commonCtrl"
    })
    .when("/home_page", {
        title: 'Home title',
        templateUrl : "partials/main.html",
        controller : "commonCtrl"
    })
    .when("/gallery_category", {
        title: 'Gallery title',
        templateUrl : "partials/gallery_category.html",
        controller : "galleryCategoryCtrl"
    })
    //dynamic content page
    .when("/gallery/category/:id", {
        title: 'Gallery category title',
        templateUrl : "partials/gallery.html",
        controller : "galleryCtrl"
    })
    .when('/page/:name', {
        title: 'Static Page title',
        templateUrl: 'partials/static_page.html',
        controller : "staticCtrl"
    })
    .when('/online_shopping/category', {
        title: 'online shop Page',
        templateUrl: 'partials/online_shop_category_list.html',
        controller : "OnlineShopCategoriesCtrl"
    })
    .when('/online_shopping/category/:id', {
        title: 'Product list Page',
        templateUrl: 'partials/online_shop_product_list.html',
        controller : "OnlineShopSingleCategoryCtrl"
    })
    .when('/online_cart', {
        title: 'cart Page',
        templateUrl: 'partials/online_cart.html',
        controller : "OnlineShopCartCategoryCtrl"
    })
    .when('/online_customer_login', {
        title: 'Customer Login',
        templateUrl: 'partials/online_customer_login.html',
        controller : "OnlineShopCustomerLoginCtrl"
    })
    .when('/online_customer_register', {
        title: 'Customer Register',
        templateUrl: 'partials/online_customer_register.html',
        controller : "OnlineShopCustomerRegisterCtrl"
    })
    .when('/online_product_checkout', {
        title: 'checkout',
        templateUrl: 'partials/online_product_checkout.html',
        controller : "OnlineShopProductCheckoutCtrl"
    })
    .when('/static_page_test', {
        title: 'static Title',
        templateUrl: 'partials/test.html'
    })
    .when('/contact', {
        title: 'contact Title',
        templateUrl: 'partials/contact.html'
    })

    .otherwise({redirectTo:'/'});
});

app.controller('commonCtrl', function($scope, $route, $http, $location, $rootScope) {
        //Load menu values from config.JSON
        $http.get("config.json")
        .success(function(client_id){
            $scope.client_id = client_id;
            $http.post("load_values.php",$scope.client_id)
            .success(function(response){
                $scope.client = response.client_details;
                $scope.menus = response.menu_arrays;
                $scope.static_pages = response.static_page_arrays;
                $scope.on_shop_categories = response.on_shop_category_arrays;
                $scope.site_details = response.site_details;

                
                $scope.cart_menu = (localStorage.getItem('cart_items')!==null && localStorage.getItem('cart_items')!=='[]') ? 1 : 0;

                $scope.customer_id = localStorage.getItem("customer_id");
            });
        }); 

        $scope.set_color = { "color" : "red",
        "background-color" : "coral",
        "font-size" : "60px",
        "padding" : "50px"
    };

    $scope.set_styles = function(color_name) {
        // if (1) {
            return {'color': color_name };
            // return {'background-image': 'url('+ example_expression+')'};
        // }
    // $scope.set_color = {'color':'red'};
    };

    $scope.customer_logout = function() {
        // alert("logout");
        // $localStorage.$reset();
        localStorage.clear();
        $http.post("online_customer_logout.php")
        .success(function(response){
            $route.reload();
            // $location.path('home_page');
        });
    };
});

app.controller('staticCtrl', function($scope, $http, $location, $rootScope) {
    //Load menu values from JSON
    $http.get("config.json")
    .success(function(client_id){

        var client_obj = client_id;
        // var absUrl = $location.absUrl();  //http://localhost/my_new_app/#/page/page1
        var path = $location.path();  ///page/page1

        var path_obj = { path: path};

        $scope.content = angular.extend(path_obj, client_obj);

        $http.post("page_content.php",$scope.content)
        .success(function(response){
            $scope.page_title = $rootScope.title;
            $scope.static_pages = response;
            // plain message
            $scope.message = "This is a Static page content";
        });

    });
});

app.controller('galleryCategoryCtrl', function($scope, $http, $location, $rootScope, $window) {

   $http.get("config.json")
   .success(function(client_id){
    $scope.client_id = client_id;
    $http.post("load_values.php",$scope.client_id)
    .success(function(response){
        $scope.gallery_categories = response.gallery_category_arrays;
    });
}); 

});

app.controller('galleryCtrl', function($scope, $http, $location, $rootScope, $window) {

    $http.get("config.json")
    .success(function(client_id){

        var client_obj = client_id;
        // var absUrl = $location.absUrl();  //http://localhost/my_new_app/#/page/page1
        var path = $location.path();  ///page/page1

        var path_obj = { path: path};

        $scope.content = angular.extend(path_obj, client_obj);

        $http.post("get_gallery.php",$scope.content)
        .success(function(response){
            $scope.page_title = $rootScope.title;
            $scope.gallery_arrays = response;
        });

    });

});

app.controller('OnlineShopCategoriesCtrl', function($scope, $http, $location, $rootScope) {
   $http.get("config.json")
   .success(function(client_id){
    $scope.client_id = client_id;
    $http.post("load_values.php",$scope.client_id)
    .success(function(response){
        $scope.on_shop_categories = response.on_shop_category_arrays;
    });
}); 
    });

app.controller('OnlineShopSingleCategoryCtrl', function($scope, $http, $location, $rootScope, $window) {

    $http.get("config.json")
    .success(function(client_id){

        var client_obj = client_id;
        // var absUrl = $location.absUrl();  //http://localhost/my_new_app/#/page/page1
        var path = $location.path();  ///page/page1

        var path_obj = { path: path};

        $scope.content = angular.extend(path_obj, client_obj);

        $http.post("get_products.php",$scope.content)
        .success(function(response){
            $scope.page_title = $rootScope.title;
            $scope.product_arrays = response;
            // plain message
            $scope.message = "This is a Static page content";
        });

    }); 

    // Clear cart items from Local storage items .
    // $scope.cart_items = [];
    // localStorage.removeItem('cart_items');
    // localStorage.clear();

    $scope.saved = localStorage.getItem('cart_items');

    $scope.cart_items = (localStorage.getItem('cart_items')!==null) ? JSON.parse($scope.saved) : [
    //{"product_id":"1","product_qty":3}
    ];

    localStorage.setItem('cart_items', JSON.stringify($scope.cart_items));

    $scope.add_to_cart = function(product_id,product_qty) {

        var jsonObj = JSON.parse(localStorage.getItem('cart_items'));

        $scope.product_id = product_id;
        $scope.product_qty = product_qty;

        var found = false;
        for (var i=0; i<jsonObj.length; i++) {
            if (jsonObj[i].product_id == product_id) {
                found = true;
                if (product_qty <= 0) {
                    // alert("quantityzerp"+i);
                    $scope.cart_items.splice(i, 1);
                    localStorage.setItem('cart_items', JSON.stringify($scope.cart_items));
                    break;
                }
                else
                {
                    jsonObj[i].product_qty = product_qty;
                    localStorage.setItem('cart_items', JSON.stringify(jsonObj));
                    break;
                }
            }
        }
        if(!found)
        {
            $scope.cart_items.push({
                product_id: product_id,
                product_qty: product_qty,
            });

            $scope.product_id = ''; //clear the input after adding
            localStorage.setItem('cart_items', JSON.stringify($scope.cart_items));
        }
        // $scope.saved = localStorage.getItem('cart_items');
        // alert("Final result"+$scope.saved);
    };

});

app.controller('OnlineShopCartCategoryCtrl', function($scope, $route, $http, $location, $rootScope, $window) {

  $scope.saved = localStorage.getItem('cart_items');
  $http.post("online_cart_items.php",$scope.saved)
  .success(function(response){
            // $scope.page_title = $rootScope.title;
            $scope.product_cart_item_arrays = response;
        });  

    $scope.cart_item_value = (localStorage.getItem('cart_items')!==null && localStorage.getItem('cart_items')!=='[]') ? 1 : 0;
    
        // This also a correct
        // $scope.getTotal = function(){
        //     var total = 0;
        //     for(var i = 0; i < $scope.product_cart_item_arrays.length; i++){
        //         var product = $scope.product_cart_item_arrays[i];
        //         total += (product.product_price * product.product_qty);
        //     }
        //     return total;
        // }

        //Get total price in cart page.
        $scope.getTotal = function(){
            var total = 0;
            angular.forEach($scope.product_cart_item_arrays, function(cart_item){
              total += cart_item.product_price * cart_item.product_qty;
          })
            localStorage.setItem('total_amount',total);
            return total;
        };

        $scope.removeCartItem = function(product_id){
            $scope.cart_items = (localStorage.getItem('cart_items')!==null) ? JSON.parse($scope.saved) : [];
            $scope.product_id = product_id;
            var jsonObj = JSON.parse(localStorage.getItem('cart_items'));
            for (var i=0; i<jsonObj.length; i++) {
                if (jsonObj[i].product_id == product_id) {
                    $scope.cart_items.splice(i, 1);
                    localStorage.setItem('cart_items', JSON.stringify($scope.cart_items));
                    $route.reload();
                    break;
                }
            }
        };

        $scope.empty_cart = function() {
           // Clear cart items from Local storage items .
           $scope.cart_items = [];
           localStorage.removeItem('cart_items');
           localStorage.clear();
       };

       $scope.checkout_items = function() {
            // alert("Email sent");
            $scope.customer_id = localStorage.getItem("customer_id");
            if($scope.customer_id !='' && $scope.customer_id != null)
            {
                $location.path('online_product_checkout');
            }
            else
            {
                $location.path('online_customer_login');
            }
        };

        $scope.mail_send = function()
        {
            if(localStorage.getItem('cart_items')!==null && localStorage.getItem('cart_items')!==[])
            {
             $scope.saved = localStorage.getItem('cart_items');
             $http.post("checkout_mail.php",$scope.saved)
             .success(function(response){
                //alert(response);
            }); 

         }
     }

 });

app.controller('OnlineShopCustomerLoginCtrl', function($scope, $route, $http, $location, $rootScope, $window) {

    $scope.customer_login = function()
    {
        $http.post("online_customer_login.php",$scope.customer)
        .success(function(response){
            var customer_id = response.id;
            var firstname = response.firstname;
            var lastname = response.lastname;
            var client_id = response.client_id;
            var email = response.email;
            
            localStorage.setItem("customer_id",customer_id);
            localStorage.setItem("client_id",client_id);
            localStorage.setItem("firstname",firstname);
            localStorage.setItem("lastname",lastname);
            localStorage.setItem("email",email);

            $scope.customer_id = localStorage.getItem("customer_id");
            if($scope.customer_id !='')
            {
                $location.path('home_page');
            }
            else
            {
                $location.path('online_customer_login');
            }

        });
    };
});

app.controller('OnlineShopCustomerRegisterCtrl', function($scope, $http, $location, $rootScope, $window) {
    $scope.customer_sign_up = function()
    {
        $http.get("config.json")
        .success(function(client){
            $scope.client_id = client.client_id;
            $scope.customer.client_id = $scope.client_id;
            $http.post("online_customer_sign_up.php",$scope.customer)
            .success(function(response){
                $scope.success = response;
                $location.path('home_page');
            });
        });
    };
});

app.controller('OnlineShopProductCheckoutCtrl', function($scope, $http, $location, $rootScope, $window) {

    $scope.payment_gateway = function()
    {
        $http.post("online_customer_lnfo_update.php",$scope.customer)
        .success(function(response){
            $scope.success = response;

            $scope.saved = localStorage.getItem('cart_items');
            $http.post("online_customer_payment.php",$scope.saved)
                .success(function(response){
                $scope.success = response;
            });
        });
    };

    $scope.shipping_address_same = function(shipping_address_checked)
    {
        if(shipping_address_checked)
        {
            $scope.customer.shipping_firstname = $scope.customer.firstname;
            $scope.customer.shipping_lastname = $scope.customer.lastname;
            $scope.customer.shipping_address = $scope.customer.address;
            $scope.customer.shipping_city = $scope.customer.city;
            $scope.customer.shipping_state = $scope.customer.state;
            $scope.customer.shipping_country = $scope.customer.country;
            $scope.customer.shipping_zipcode = $scope.customer.zipcode;
            $scope.customer.shipping_phone = $scope.customer.phone;
        }
        else
        {
            $scope.customer.shipping_firstname = '';
            $scope.customer.shipping_lastname = '';
            $scope.customer.shipping_address = '';
            $scope.customer.shipping_city = '';
            $scope.customer.shipping_state = '';
            $scope.customer.shipping_country = '';
            $scope.customer.shipping_zipcode = '';
            $scope.customer.shipping_phone = '';

        }

    };

});

// app.controller('OnlineShopProductCheckoutCtrl', function($scope, $http, $location, $rootScope, $window) {

// });




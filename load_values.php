<?php
include('config.php');
	// Check connection
if (!$conn) {
	die("Connection failed: " . mysqli_connect_error());
}
else
{
	$request = json_decode( file_get_contents('php://input') );
	$client_id = $request->client_id;
	$active_status = 1; //Active status


	// clients for menu
	$sql = "SELECT * FROM clients WHERE id = $client_id && client_status =$active_status";
	$result = mysqli_query($conn, $sql);
	// $static_page_array = array();
	$client_details = mysqli_fetch_assoc($result);
	$response["client_details"] = $client_details;


	// settings for client details
	$sql = "SELECT * FROM settings AS s LEFT JOIN clients AS c ON c.id = s.client_id WHERE s.client_id = $client_id && s.setting_status=$active_status && c.client_status=$active_status";
	$result = mysqli_query($conn, $sql);
	$setting_array = array();
	while($row = mysqli_fetch_assoc($result))
	{
		$setting_array[] = $row;
	}
	$response["site_details"] = $setting_array;


	// Client menu permission
	$sql = "SELECT m.name, m.url, m.icon FROM menu AS m INNER JOIN client_menu_permisssion AS cp ON m.id = cp.menu_id INNER JOIN clients AS c ON c.id = cp.client_id WHERE cp.client_id=$client_id && cp.status=$active_status && c.client_status=$active_status";
	$result = mysqli_query($conn, $sql);
	$menu_array = array();
	while($row = mysqli_fetch_assoc($result)){	
		$menu_array[] = $row;
	}
	$response["menu_arrays"] = $menu_array;


	// static_pages for menu
	$sql = "SELECT * FROM static_pages AS sp INNER JOIN clients AS c ON c.id = sp.client_id WHERE client_id = $client_id AND c.client_status=$active_status AND sp.page_status=$active_status ORDER BY page_sort ASC";
	$result = mysqli_query($conn, $sql);
	$static_page_array = array();
	while($row = mysqli_fetch_assoc($result)){	
		$static_page_array[] = $row;
	}
	$response["static_page_arrays"] = $static_page_array;


	// category for menu
	$sql = "SELECT * FROM category AS cat INNER JOIN clients AS c ON c.id = cat.client_id WHERE cat.client_id = $client_id AND c.client_status = $active_status AND cat.category_status = $active_status";

	$result = mysqli_query($conn, $sql);
	$on_shop_category_array = array();
	while($row = mysqli_fetch_assoc($result)){	
		$on_shop_category_array[] = $row;
	}
	$response["on_shop_category_arrays"] = $on_shop_category_array;

	// gallery_category for menu
	$sql = "SELECT * FROM gallery_category AS gc INNER JOIN clients AS c ON c.id = gc.client_id WHERE client_id = $client_id && gc.gallery_category_status = $active_status && c.client_status=$active_status";
	$result = mysqli_query($conn, $sql);
	$gallery_category_array = array();
	while($row = mysqli_fetch_assoc($result)){	
		$gallery_category_array[] = $row;
	}
	$response["gallery_category_arrays"] = $gallery_category_array;

	echo json_encode($response);
}
?>
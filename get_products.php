<?php
include('config.php');
	// Check connection
if (!$conn) {
	die("Connection failed: " . mysqli_connect_error());
}
else
{
	$request = json_decode( file_get_contents('php://input') );
	$client_id = $request->client_id;
	$path = $request->path;
	$page_url = explode("/",$path);
	$category_id = $page_url[3];

	$sql = "SELECT * FROM products AS p INNER JOIN category AS c ON c.category_id = p.category_id WHERE c.client_id=$client_id && p.category_id=$category_id";

	$result = mysqli_query($conn, $sql);

	$product_array = array();
	while($row = mysqli_fetch_assoc($result)){	
		$product_array[] = $row;
	}

	echo json_encode($product_array);
}
?>
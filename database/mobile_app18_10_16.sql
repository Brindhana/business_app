-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 18, 2016 at 10:33 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mobile_app`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `category_id` int(11) NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `parent_category_id` int(10) DEFAULT NULL,
  `category_name` varchar(100) DEFAULT NULL,
  `category_image` varchar(50) DEFAULT NULL,
  `category_status` int(5) DEFAULT NULL,
  `category_created_at` timestamp NULL DEFAULT NULL,
  `category_modified_at` timestamp NULL DEFAULT NULL,
  `category_created_by` int(11) DEFAULT NULL,
  `category_modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `client_id`, `parent_category_id`, `category_name`, `category_image`, `category_status`, `category_created_at`, `category_modified_at`, `category_created_by`, `category_modified_by`) VALUES
(1, 1, NULL, 'Category one', 'banner4.jpg', 1, NULL, NULL, NULL, NULL),
(2, 1, NULL, 'Category two', 'banner3.jpg', 1, NULL, NULL, NULL, NULL),
(3, 1, NULL, 'category Three', 'banner2.jpg', 1, NULL, NULL, NULL, NULL),
(4, 1, NULL, 'Category Four', '5.jpg', 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL,
  `image` varchar(30) DEFAULT NULL,
  `logo` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `modified_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `name`, `email`, `password`, `image`, `logo`, `created_at`, `modified_at`) VALUES
(1, 'user', 'user@gmail.com', '12345', 'user.jpg', NULL, '2016-09-08 18:30:00', '2016-09-14 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `client_menu_permisssion`
--

CREATE TABLE `client_menu_permisssion` (
  `id` int(11) NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `menu_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `modified_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client_menu_permisssion`
--

INSERT INTO `client_menu_permisssion` (`id`, `client_id`, `menu_id`, `status`, `created_at`, `modified_at`) VALUES
(1, 1, 1, 1, '2016-08-31 18:30:00', '2016-09-08 18:30:00'),
(2, 1, 2, 1, '2016-09-09 18:30:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `gallery_id` int(10) NOT NULL,
  `client_id` int(10) DEFAULT NULL,
  `gallery_category_id` int(10) DEFAULT NULL,
  `gallery_name` varchar(200) DEFAULT NULL,
  `gallery_image` varchar(200) DEFAULT NULL,
  `gallery_description` varchar(250) DEFAULT NULL,
  `is_gallery_featured` tinyint(1) DEFAULT NULL,
  `gallery_status` int(2) DEFAULT NULL,
  `gallery_created_at` timestamp NULL DEFAULT NULL,
  `gallery_modified_at` timestamp NULL DEFAULT NULL,
  `gallery_created_by` int(10) DEFAULT NULL,
  `gallery_modified_by` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`gallery_id`, `client_id`, `gallery_category_id`, `gallery_name`, `gallery_image`, `gallery_description`, `is_gallery_featured`, `gallery_status`, `gallery_created_at`, `gallery_modified_at`, `gallery_created_by`, `gallery_modified_by`) VALUES
(1, 1, 1, 'Tiger', '1.jpg', 'This is a category 1 gallery(Animal)', NULL, 1, NULL, NULL, NULL, NULL),
(2, 1, 1, 'Dove', '2.jpg', 'This is a Gallery category 2(Birds)', NULL, 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `gallery_category`
--

CREATE TABLE `gallery_category` (
  `gallery_category_id` int(11) NOT NULL,
  `client_id` int(10) DEFAULT NULL,
  `parent_gallery_category_id` int(10) DEFAULT NULL,
  `gallery_category_name` varchar(100) DEFAULT NULL,
  `gallery_category_image` varchar(200) DEFAULT NULL,
  `gallery_category_status` int(2) DEFAULT NULL,
  `gallery_category_created_at` timestamp NULL DEFAULT NULL,
  `gallery_category_modified_at` timestamp NULL DEFAULT NULL,
  `gallery_category_created_by` int(10) DEFAULT NULL,
  `gallery_category_modified_by` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery_category`
--

INSERT INTO `gallery_category` (`gallery_category_id`, `client_id`, `parent_gallery_category_id`, `gallery_category_name`, `gallery_category_image`, `gallery_category_status`, `gallery_category_created_at`, `gallery_category_modified_at`, `gallery_category_created_by`, `gallery_category_modified_by`) VALUES
(1, 1, NULL, 'Gallery Category 1', 'banner4.jpg', 1, NULL, NULL, NULL, NULL),
(2, 1, NULL, 'Gallery category 2', 'banner3.jpg', 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `icon` varchar(100) DEFAULT NULL,
  `template` varchar(100) DEFAULT NULL,
  `controller` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `name`, `url`, `icon`, `template`, `controller`) VALUES
(1, 'Home', 'home_page', 'ion-android-home', 'home.html', NULL),
(2, 'Gallery Categories', 'gallery_category', 'ion-ios-videocam', 'gallery.html', 'galleryCtrl'),
(3, 'Plain Page', 'plain_page', 'ion-android-map', 'plain_page.html', 'plainPageCtrl');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` int(10) NOT NULL,
  `category_id` int(10) DEFAULT NULL,
  `product_name` varchar(100) DEFAULT NULL,
  `product_image` varchar(50) DEFAULT NULL,
  `product_price` float DEFAULT NULL,
  `product_unit` varchar(30) DEFAULT NULL,
  `product_description` text,
  `is_featured` tinyint(1) DEFAULT NULL,
  `product_status` int(2) DEFAULT NULL,
  `product_created_at` timestamp NULL DEFAULT NULL,
  `product_modified_at` timestamp NULL DEFAULT NULL,
  `product_created_by` int(10) DEFAULT NULL,
  `product_modified_by` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `category_id`, `product_name`, `product_image`, `product_price`, `product_unit`, `product_description`, `is_featured`, `product_status`, `product_created_at`, `product_modified_at`, `product_created_by`, `product_modified_by`) VALUES
(1, 1, 'Mountain', '1.jpg', 2000, NULL, 'Super Table', 1, 1, NULL, NULL, NULL, NULL),
(2, 1, 'Tower', '2.jpg', 300, NULL, 'product 2 discription', 1, 0, NULL, NULL, NULL, NULL),
(3, 2, 'Street view', '3.jpg', 20000, NULL, 'Street description', 0, 1, NULL, NULL, NULL, NULL),
(4, 2, 'Cave', '4.jpg', NULL, NULL, 'Cave Description', 1, 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `static_pages`
--

CREATE TABLE `static_pages` (
  `page_id` int(11) NOT NULL,
  `client_id` int(10) DEFAULT NULL,
  `page_name` varchar(50) DEFAULT NULL,
  `page_url` varchar(50) DEFAULT NULL,
  `page_icon` varchar(50) DEFAULT NULL,
  `page_content` text,
  `page_status` int(3) DEFAULT NULL,
  `page_sort` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `static_pages`
--

INSERT INTO `static_pages` (`page_id`, `client_id`, `page_name`, `page_url`, `page_icon`, `page_content`, `page_status`, `page_sort`) VALUES
(1, 1, 'Static page', 'page1', 'ion-android-playstore', 'some content some contetn dijfsoad jfosadj foi', 1, 1),
(2, 1, 'secondpage', 'page2', 'ion-android-playstore', 'page2 ', 1, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client_menu_permisssion`
--
ALTER TABLE `client_menu_permisssion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`gallery_id`);

--
-- Indexes for table `gallery_category`
--
ALTER TABLE `gallery_category`
  ADD PRIMARY KEY (`gallery_category_id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `static_pages`
--
ALTER TABLE `static_pages`
  ADD PRIMARY KEY (`page_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `client_menu_permisssion`
--
ALTER TABLE `client_menu_permisssion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `gallery_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `gallery_category`
--
ALTER TABLE `gallery_category`
  MODIFY `gallery_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `static_pages`
--
ALTER TABLE `static_pages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

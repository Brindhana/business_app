-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 10, 2016 at 05:49 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mobile_app`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `category_id` int(11) NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `parent_category_id` int(10) DEFAULT NULL,
  `category_name` varchar(100) DEFAULT NULL,
  `category_image` varchar(50) DEFAULT NULL,
  `category_description` varchar(200) DEFAULT NULL,
  `category_status` int(5) DEFAULT NULL,
  `category_created_at` timestamp NULL DEFAULT NULL,
  `category_modified_at` timestamp NULL DEFAULT NULL,
  `category_created_by` int(11) DEFAULT NULL,
  `category_modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `client_id`, `parent_category_id`, `category_name`, `category_image`, `category_description`, `category_status`, `category_created_at`, `category_modified_at`, `category_created_by`, `category_modified_by`) VALUES
(1, 1, NULL, 'Formal Coat', 'banner4.jpg', '10 - 50% Offer ', 1, NULL, NULL, NULL, NULL),
(2, 1, NULL, 'Shoes', 'banner3.jpg', '10 - 50% Offer ', 1, NULL, NULL, NULL, NULL),
(3, 1, NULL, 'category Three', 'banner2.jpg', NULL, 0, NULL, NULL, NULL, NULL),
(4, 1, NULL, 'Category Four', '5.jpg', NULL, 0, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL,
  `image` varchar(30) DEFAULT NULL,
  `logo` varchar(100) DEFAULT NULL,
  `client_status` int(2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `modified_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `name`, `email`, `password`, `image`, `logo`, `client_status`, `created_at`, `modified_at`) VALUES
(1, 'AppNoww', 'appnoww@nowwin.com', '12345', 'developer.jpg', 'dev_logo.png', 1, '2016-09-08 18:30:00', '2016-09-14 18:30:00'),
(2, 'Cricket club', 'cricket.club@gmail.com', '12345', 'cricket_club.jpg', 'cricket_club/logo.png', 1, NULL, NULL),
(3, 'Cine School', 'cine@gmail.com', '12345', 'cine.jpg', 'cine_school/logo.png', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `client_menu_permisssion`
--

CREATE TABLE `client_menu_permisssion` (
  `id` int(11) NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `menu_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `modified_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client_menu_permisssion`
--

INSERT INTO `client_menu_permisssion` (`id`, `client_id`, `menu_id`, `status`, `created_at`, `modified_at`) VALUES
(1, 1, 1, 1, '2016-08-31 18:30:00', '2016-09-08 18:30:00'),
(2, 2, 2, 1, '2016-09-09 18:30:00', NULL),
(3, 3, 1, 1, NULL, NULL),
(4, 3, 2, 1, NULL, NULL),
(5, 1, 2, 1, NULL, NULL),
(6, 1, 4, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `gallery_id` int(10) NOT NULL,
  `client_id` int(10) DEFAULT NULL,
  `gallery_category_id` int(10) DEFAULT NULL,
  `gallery_name` varchar(200) DEFAULT NULL,
  `gallery_image` varchar(200) DEFAULT NULL,
  `gallery_description` varchar(250) DEFAULT NULL,
  `is_gallery_featured` tinyint(1) DEFAULT NULL,
  `gallery_status` int(2) DEFAULT NULL,
  `gallery_created_at` timestamp NULL DEFAULT NULL,
  `gallery_modified_at` timestamp NULL DEFAULT NULL,
  `gallery_created_by` int(10) DEFAULT NULL,
  `gallery_modified_by` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`gallery_id`, `client_id`, `gallery_category_id`, `gallery_name`, `gallery_image`, `gallery_description`, `is_gallery_featured`, `gallery_status`, `gallery_created_at`, `gallery_modified_at`, `gallery_created_by`, `gallery_modified_by`) VALUES
(1, 1, 1, 'HP', 'hp_laptop.jpg', 'This is a category 1 gallery(Animal)', NULL, 1, NULL, NULL, NULL, NULL),
(2, 1, 1, 'Dell', 'dell_laptop.jpg', 'This is a Gallery category 2(Birds)', NULL, 1, NULL, NULL, NULL, NULL),
(3, 2, 3, 'One day Match 1', 'cricket_club/oneday/oneday1.png', 'one day description', 1, 1, NULL, NULL, NULL, NULL),
(4, 2, 3, 'One day Match 2', 'cricket_club/oneday/oneday2.png', 'one day description', 1, 1, NULL, NULL, NULL, NULL),
(5, 2, 5, 'Test Match 1', 'cricket_club/test/test1.jpg', 'Test 1 description', 1, 1, NULL, NULL, NULL, NULL),
(6, 2, 5, 'Test Match 2', 'cricket_club/test/test2.jpg', 'Test 1 description', 1, 1, NULL, NULL, NULL, NULL),
(7, 1, 2, 'samsung_mobile', 'samsung_mobile.jpg', 'Best Price', 1, 1, NULL, NULL, NULL, NULL),
(8, 1, 2, 'CoolPad', 'coolpad.jpg', 'cool_pad ', 1, 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `gallery_category`
--

CREATE TABLE `gallery_category` (
  `gallery_category_id` int(11) NOT NULL,
  `client_id` int(10) DEFAULT NULL,
  `parent_gallery_category_id` int(10) DEFAULT NULL,
  `gallery_category_name` varchar(100) DEFAULT NULL,
  `gallery_category_image` varchar(200) DEFAULT NULL,
  `gallery_category_description` varchar(200) DEFAULT NULL,
  `gallery_category_status` int(2) DEFAULT NULL,
  `gallery_category_created_at` timestamp NULL DEFAULT NULL,
  `gallery_category_modified_at` timestamp NULL DEFAULT NULL,
  `gallery_category_created_by` int(10) DEFAULT NULL,
  `gallery_category_modified_by` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery_category`
--

INSERT INTO `gallery_category` (`gallery_category_id`, `client_id`, `parent_gallery_category_id`, `gallery_category_name`, `gallery_category_image`, `gallery_category_description`, `gallery_category_status`, `gallery_category_created_at`, `gallery_category_modified_at`, `gallery_category_created_by`, `gallery_category_modified_by`) VALUES
(1, 1, NULL, 'Laptop', 'laptop_category.jpg', 'Wonderful Touch Screen Display', 1, NULL, NULL, NULL, NULL),
(2, 1, NULL, 'Mobile', 'mobile_category.jpg', 'Latest Smart Phones', 1, NULL, NULL, NULL, NULL),
(3, 2, NULL, 'One Day', 'cricket_club/oneday.png', NULL, 1, NULL, NULL, NULL, NULL),
(4, 2, NULL, 'T-20', 'cricket_club/test.png', NULL, 0, NULL, NULL, NULL, NULL),
(5, 2, NULL, 'Test', 'cine_school/', NULL, 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `icon` varchar(100) DEFAULT NULL,
  `template` varchar(100) DEFAULT NULL,
  `controller` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `name`, `url`, `icon`, `template`, `controller`) VALUES
(1, 'Home', 'home_page', 'ion-android-home', 'home.html', NULL),
(2, 'Gallery', 'gallery_category', 'ion-ios-videocam', 'gallery.html', 'galleryCtrl'),
(3, 'Contact', 'contact_us', 'ion-android-map', 'plain_page.html', 'plainPageCtrl'),
(4, 'Online Shop', 'online_shopping/category', 'ion-android-map', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `online_customers`
--

CREATE TABLE `online_customers` (
  `id` int(11) NOT NULL,
  `client_id` int(10) DEFAULT NULL,
  `firstname` varchar(100) DEFAULT NULL,
  `lastname` varchar(100) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `phone` bigint(15) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `city` varchar(200) DEFAULT NULL,
  `state` varchar(200) DEFAULT NULL,
  `country` varchar(200) DEFAULT NULL,
  `zipcode` bigint(15) DEFAULT NULL,
  `shipping_firstname` varchar(100) DEFAULT NULL,
  `shipping_lastname` varchar(100) DEFAULT NULL,
  `shipping_phone` bigint(12) DEFAULT NULL,
  `shipping_address` varchar(250) DEFAULT NULL,
  `shipping_city` varchar(200) DEFAULT NULL,
  `shipping_state` varchar(200) DEFAULT NULL,
  `shipping_country` varchar(200) DEFAULT NULL,
  `shipping_zipcode` bigint(15) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `modified_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `online_customers`
--

INSERT INTO `online_customers` (`id`, `client_id`, `firstname`, `lastname`, `email`, `password`, `phone`, `address`, `city`, `state`, `country`, `zipcode`, `shipping_firstname`, `shipping_lastname`, `shipping_phone`, `shipping_address`, `shipping_city`, `shipping_state`, `shipping_country`, `shipping_zipcode`, `created_at`, `modified_at`) VALUES
(1, 1, 'Brindha', 'Dhanasekar', 'brin@gmail.com', '1234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-11-07 23:23:58', NULL),
(2, NULL, 'Dhana', 'Sekar', 'dhana@gmail.com', 'dhana', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-11-07 23:34:42', NULL),
(3, 1, 'brindha', 'Nowwin', 'brindha.nowwin@gmail.com', '12345!@#$%', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-11-07 23:42:59', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `online_orders`
--

CREATE TABLE `online_orders` (
  `order_id` int(10) NOT NULL,
  `client_id` int(10) DEFAULT NULL,
  `customer_id` int(10) DEFAULT NULL,
  `total` float DEFAULT NULL,
  `order_date` timestamp NULL DEFAULT NULL,
  `order_status` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `online_orders`
--

INSERT INTO `online_orders` (`order_id`, `client_id`, `customer_id`, `total`, `order_date`, `order_status`) VALUES
(1, 0, 0, 3000, '2016-11-09 07:46:23', 0),
(2, 0, 1, 3000, '2016-11-09 07:49:01', 0),
(3, 0, 1, 3000, '2016-11-09 07:49:46', 0),
(4, 0, 1, 3000, '2016-11-09 07:51:24', 0),
(5, 0, 1, 3000, '2016-11-09 08:02:00', 0),
(6, 0, 1, 3000, '2016-11-09 08:19:59', 0),
(7, 0, 1, 3000, '2016-11-09 08:30:57', 0),
(8, 0, 1, 3000, '2016-11-09 08:50:29', 0),
(9, 0, 1, 3000, '2016-11-09 08:51:46', 0);

-- --------------------------------------------------------

--
-- Table structure for table `online_order_items`
--

CREATE TABLE `online_order_items` (
  `order_item_id` int(11) NOT NULL,
  `order_id` int(10) DEFAULT NULL,
  `product_id` int(10) DEFAULT NULL,
  `product_qty` int(5) DEFAULT NULL,
  `order_item_status` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` int(10) NOT NULL,
  `category_id` int(10) DEFAULT NULL,
  `product_name` varchar(100) DEFAULT NULL,
  `product_image` varchar(50) DEFAULT NULL,
  `product_price` float DEFAULT NULL,
  `product_unit` varchar(30) DEFAULT NULL,
  `product_description` text,
  `is_featured` tinyint(1) DEFAULT NULL,
  `product_status` int(2) DEFAULT NULL,
  `product_created_at` timestamp NULL DEFAULT NULL,
  `product_modified_at` timestamp NULL DEFAULT NULL,
  `product_created_by` int(10) DEFAULT NULL,
  `product_modified_by` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `category_id`, `product_name`, `product_image`, `product_price`, `product_unit`, `product_description`, `is_featured`, `product_status`, `product_created_at`, `product_modified_at`, `product_created_by`, `product_modified_by`) VALUES
(1, 1, 'Slim Fit Casual ', 'coat1.jpg', 2000, NULL, 'Gray color casual coat, has a spread collar, button placket, long sleeves, a patch pocket, curved hem', 1, 1, NULL, NULL, NULL, NULL),
(2, 1, 'Raymond blazers', 'coat2.jpg', 300, NULL, 'A blazer is a type of jacket resembling a suit jacket, but cut more casually.', 1, 0, NULL, NULL, NULL, NULL),
(3, 2, 'WILTON WEEJUNS BROWN', 'shoe1.jpg', 1500, NULL, 'Maroon color training shoes for men that will certainly take your training sessions to the next level', 0, 1, NULL, NULL, NULL, NULL),
(4, 2, 'Boat Shoe', 'shoe2.jpg', 1000, NULL, 'A siping pattern is cut into the soles to provide grip on a wet deck; the leather construction, along with application of oil, is designed to repel water; and the stitching is highly durable', 1, 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `static_pages`
--

CREATE TABLE `static_pages` (
  `page_id` int(11) NOT NULL,
  `client_id` int(10) DEFAULT NULL,
  `page_name` varchar(50) DEFAULT NULL,
  `page_url` varchar(50) DEFAULT NULL,
  `page_icon` varchar(50) DEFAULT NULL,
  `page_content` text,
  `page_status` int(3) DEFAULT NULL,
  `page_sort` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `static_pages`
--

INSERT INTO `static_pages` (`page_id`, `client_id`, `page_name`, `page_url`, `page_icon`, `page_content`, `page_status`, `page_sort`) VALUES
(1, 1, 'Coming soon', 'plain_page', 'ion-android-playstore', ' <!-- Page Content -->\n      <div id="content">\n\n        <!-- Main Content -->\n        <div class="animated fadein bg-opacity">\n          <div class="valign-wrapper fullscreen">\n            <!-- Page Center -->\n            <div class="valign">\n              <div class="row">\n                <div class="col s12 center-align animated fadein delay-1">\n                  <h1 class="title white-text">Coming Soon</h1>\n                </div>\n                <div class="col s12 center-align p-20 animated fadeinup delay-2">\n                  <h2 id="countdown"></h2>\n                  <span class="white-text">We''re currently working on creating something fantastic.<br>\n                  We''ll be here soon, subscribe to be notified</span>\n                  <div class="input-field animated fadein delay-4">\n                    <input id="icon_prefix" type="email" class="center-align validate" placeholder="Enter your email address">\n                    <a href="index.html" class="waves-effect waves-light btn primary-color">Send</a>\n                  </div>\n                </div>\n              </div>\n            </div>\n\n            <!-- Footer -->\n          </div>\n        </div> <!-- End of Main Contents -->\n\n         \n      </div> <!-- End of Page Content -->\n    ', 0, 3),
(2, 1, 'Blog', 'blog', 'ion-android-playstore', ' <!-- Page Content -->\n <div id="content" class="page">\n  <div class="parallax">\n\n    <!-- Toolbar -->\n    <div id="toolbar" class="primary-color">\n      <div class="open-left" id="open-left" data-activates="slide-out-left">\n        <i class="ion-android-menu"></i>\n      </div>\n      <span class="title">Blog</span>\n      <div class="open-right" id="open-right" data-activates="slide-out">\n        <i class="ion-android-person"></i>\n      </div>\n    </div>\n  </div>\n  \n  <!-- Main Content -->\n  <div class="animated fadeinup">\n\n    <div class="blog-fullwidth animated fadeinup delay-1">\n      <div class="blog-header">\n        <img class="avatar circle" src="img/user4.jpg" alt="">\n        <div class="blog-author">\n          <span>Jassie North</span>\n          <span class="small">1hour ago - 62 Share</span>\n        </div>\n      </div>\n      <div class="blog-image">\n        <img src="img/8.jpg" alt="">\n        <div class="opacity-overlay-top"></div>\n      </div>\n      <div class="blog-preview p-20">\n        <h4 class="uppercase">It really makes no sense</h4>\n        <p>This is dummy caption. It has been placed here solely to demonstrate the look and feel of finished, typeset text.</p>\n        <a href="article.html" class="waves-effect waves-light btn primary-color">Read</a>\n      </div>\n    </div>\n\n    <div class="blog-fullwidth animated fadeinup delay-3">\n      <div class="blog-header">\n        <img class="avatar circle" src="img/user2.jpg" alt="">\n        <div class="blog-author">\n          <span>Jassie North</span>\n          <span class="small">1hour ago - 62 Share</span>\n        </div>\n      </div>\n      <div class="blog-image">\n        <img src="img/5.jpg" alt="">\n        <div class="opacity-overlay-top"></div>\n      </div>\n      <div class="blog-preview p-20">\n        <h4 class="uppercase">It really makes no sense</h4>\n        <p>This is dummy caption. It has been placed here solely to demonstrate the look and feel of finished, typeset text.</p>\n        <a href="article.html" class="waves-effect waves-light btn primary-color">Read</a>\n      </div>\n    </div>\n    \n    <div class="blog-fullwidth animated fadeinup delay-5">\n      <div class="blog-header">\n        <img class="avatar circle" src="img/user3.jpg" alt="">\n        <div class="blog-author">\n          <span>Jassie North</span>\n          <span class="small">1hour ago - 62 Share</span>\n        </div>\n      </div>\n      <div class="blog-image">\n        <img src="img/9.jpg" alt="">\n        <div class="opacity-overlay-top"></div>\n      </div>\n      <div class="blog-preview p-20">\n        <h4 class="uppercase">It really makes no sense</h4>\n        <p>This is dummy caption. It has been placed here solely to demonstrate the look and feel of finished, typeset text.</p>\n        <a href="article.html" class="waves-effect waves-light btn primary-color">Read</a>\n      </div>\n    </div>\n\n    <!-- Footer -->\n\n  </div> <!-- End of Main Contents -->\n  \n  \n      </div> <!-- End of Page Content -->', 0, 2),
(3, 2, 'Contact us', 'contact_us', 'ion-android-map', '<!-- Page Content -->\n      <div id="content" class="page grey-blue">\n      <div class="parallax">\n\n        <!-- Toolbar -->\n        <div id="toolbar" class="primary-color">\n          <div class="open-left" id="open-left" data-activates="slide-out-left">\n            <i class="ion-android-menu"></i>\n          </div>\n          <span class="title">Contact</span>\n          <div class="open-right" id="open-right" data-activates="slide-out">\n            <i class="ion-android-person"></i>\n          </div>\n        </div>\n        </div>\n        \n        <!-- Main Content -->\n        <div class="animated fadeinup">\n          \n          <iframe class="animated fadein" width="100%" height="350px" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15546.34937329225!2d80.2783496!3d13.0619182!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x35b52533d49c4b58!2sMadras+Cricket+Club!5e0!3m2!1sen!2sin!4v1476781730199" style="border: none;"></iframe>\n\n          <!-- Form Inputs --> \n          <div class="form-inputs">\n\n            <h4 class="center">Fill the form!</h4>\n            <div>\n              <div class="input-field animated fadeinright">\n                <input id="first_name" type="text" class="validate">\n                <label for="first_name">First Name</label>\n              </div>\n              <div class="input-field animated fadeinright delay-1">\n                <input id="last_name" type="text" class="validate">\n                <label for="last_name">Last Name</label>\n              </div>\n            </div>\n            <div class="input-field animated fadeinright delay-2">\n              <input id="email" type="email">\n              <label for="email">Email</label>\n            </div>\n            <div class="input-field animated fadeinright delay-3">\n              <input id="telephone" type="text" class="validate">\n              <label for="telephone">Telephone</label>\n            </div>\n            <div class="input-field animated fadeinright delay-4">\n              <input id="city" type="text" class="validate">\n              <label for="city">City</label>\n            </div>\n            \n            <div class="input-field animated fadeinright delay-5">\n              <textarea class="materialize-textarea" id="textarea1"></textarea> \n              <label for="textarea1">Write your message here</label>\n            </div>\n\n            <p class="remember animated bouncein delay-6">\n              <input type="checkbox" id="test5" />\n              <label for="test5">This is an awesome checkbox</label>\n            </p>\n        \n            <a class="waves-effect waves-light btn-large primary-color width-100 animated bouncein delay-6" href="index.html">Send</a>\n          </div>\n\n          <!-- Footer -->\n          <footer class="page-footer primary-color">\n          <div class="container">\n            <div class="row">\n              <div class="col s12">\n                <p class="center-align grey-text text-lighten-4">You can use rows and columns here to organize your footer content.</p>\n                <div class="center-align">\n                  <i class="ion-social-facebook m-10 white-text"></i>\n                  <i class="ion-social-twitter m-10 white-text"></i>\n                  <i class="ion-social-pinterest m-10 white-text"></i>\n                  <i class="ion-social-dribbble m-10 white-text"></i>\n                </div>\n              </div>\n            </div>\n          </div>\n          <div class="footer-copyright blue darken-1">\n            <div class="container">\n            2016 Codnauts\n            <a class="grey-text text-lighten-4 right" href="#!">Privacy Policy</a>\n            </div>\n          </div>\n        </footer>\n        </div> <!-- End of Main Contents -->\n      \n       \n      </div> <!-- End of Page Content -->', 1, 3),
(4, 1, 'Article', 'article', 'ion-android-document', '<div class="container">\n<center><h2>Tamil Nadu cricket team</h2></center><br>\n<h4>Lora Bell</h4>\n<p>\nThe The Tamil Nadu cricket team is a domestic cricket team run by Tamil Nadu Cricket Association representing the state of Tamil Nadu, India. The team plays in Ranji Trophy, the top tier of the domestic first-class cricket tournament in India. Before renaming of Madras state to Tamil Nadu, the team was known as Madras until the 1970-71 season. They have won the Ranji Trophy twice and have finished runners-up nine times.The team is based at the M. A. Chidambaram Stadium, named after a former president of the BCCI. Established in 1916, it has a capacity of 50,000 and had floodlights installed in 1996.</p>\n<br>\n<blockquote>"You can cut the tension with a cricket stump."</blockquote>\n<br>\n<img src="static_page_images/team.jpg" width="100%" alt="team">\n<br>\n</div>', 1, 1),
(5, 1, 'Events', 'events', 'ion-android-list', '<div class="container">\n<center><h1>Event schedule</h1></center>\n<h3>Opening ceremony</h3>\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\n<center><h1>Speakers</h1></center>\n<h3>Jessica Ronald</h3>\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\n</div>', 1, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client_menu_permisssion`
--
ALTER TABLE `client_menu_permisssion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`gallery_id`);

--
-- Indexes for table `gallery_category`
--
ALTER TABLE `gallery_category`
  ADD PRIMARY KEY (`gallery_category_id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `online_customers`
--
ALTER TABLE `online_customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `online_orders`
--
ALTER TABLE `online_orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `online_order_items`
--
ALTER TABLE `online_order_items`
  ADD PRIMARY KEY (`order_item_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `static_pages`
--
ALTER TABLE `static_pages`
  ADD PRIMARY KEY (`page_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `client_menu_permisssion`
--
ALTER TABLE `client_menu_permisssion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `gallery_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `gallery_category`
--
ALTER TABLE `gallery_category`
  MODIFY `gallery_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `online_customers`
--
ALTER TABLE `online_customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `online_orders`
--
ALTER TABLE `online_orders`
  MODIFY `order_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `online_order_items`
--
ALTER TABLE `online_order_items`
  MODIFY `order_item_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `static_pages`
--
ALTER TABLE `static_pages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

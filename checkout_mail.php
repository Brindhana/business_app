<?php
include('config.php');
	// Check connection
if (!$conn) {
	die("Connection failed: " . mysqli_connect_error());
}
else
{
	$request = json_decode( file_get_contents('php://input'),true);
	// {"product_id":"1","product_qty":4},{"product_id":"2","product_qty":7}

	$cart_items= array();
	foreach ($request as $key => $items) {
		$product_id = $items['product_id'];

		$product_qty = $items['product_qty'];

		$sql = "SELECT * FROM products WHERE product_id = $product_id";

		$result = mysqli_query($conn, $sql);

		$product_cart_item_arrays = array();

		$row = mysqli_fetch_assoc($result);
		
		$product_name = $row['product_name'];
		$product_price = $row['product_price'];

		$product_cart_item_arrays["product_id"] = $product_id;
		$product_cart_item_arrays["product_name"] = $product_name;
		$product_cart_item_arrays["product_price"] = $product_price;
		$product_cart_item_arrays["product_qty"] = $product_qty;
		
		array_push($cart_items, $product_cart_item_arrays);
	}

	echo json_encode($cart_items);

	$message = "<!DOCTYPE html>
	<html>
	<head>
		<style>
			table {
				font-family: arial, sans-serif;
				border-collapse: collapse;
				width: 100%;
			}

			td, th {
				border: 1px solid #dddddd;
				text-align: left;
				padding: 8px;
			}
			h1
			{
				background-color: #4d90fe;
				border-radius: 3px 3px 0 0!important;
				color: #ffffff;
				font-weight: bold;
				vertical-align: middle;
				padding: 20px;
			}
			.gap
			{
				margin-top:10px;
			}
		</style>
	</head>
	<body>
		<h1>Your order is complete</h1>

		<p class='gap'>Hi there. Your recent order on AppNoww has been completed. Your order details are shown below for your reference:</p>

		<h4 class='gap'>Your order ID: #12345</h4>

		<table class='gap'>

			<tr>
				<th>S.No</th>
				<th>Product</th>
				<th>Quantity</th>
				<th>Price</th>
			</tr>";

			foreach ($cart_items AS $key => $values)
			{

				$key = $key+1;
				$message .= "<tr><td>".$key;
				$message .= "</td><td>".$values['product_name'];
				$message .= "</td><td>".$values['product_qty'];
				$message .= "</td><td>".$values['product_price'];
				$message .= "</td></tr>";
			}

			$message .= "</table> </body> </html>";


			$from = "appnoww@gmail.com";
			$replyto = "mk@gmail.com";
	// $bcc = "brindha.nowwin@gmail.com";
			$cc= "mk.nowwin@gmail.com";
			$subject = "Order Complete";
	// $content = "Your Order has been confirmed.";

			$to      = "brindha.nowwin@gmail.com";
			$subject = $subject;
			$message = $message;
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers .= 'From: '.$from. "\r\n";
			$headers .= 'Reply-To: '.$replyto."\r\n";
	// $headers .='Bcc:'.$bcc."\r\n";
			//$headers .='Cc: '.$cc."\r\n";
			$headers .= 'X-Mailer: PHP/' . phpversion();

			if(mail($to, $subject, $message, $headers))
			{
				echo "Mail has been sent.";
			}
			else
			{
				echo "Not sent";
			}
		}
		?>
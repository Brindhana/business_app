<?php
session_start();
include('config.php');
// Check connection
if (!$conn) {
	die("Connection failed: " . mysqli_connect_error());
}
else
{

		//***************** order and order_items stored ***************** 

	// $order_status = 0; //Initially it take 0, After payment it switched to 1(paid amount) 
	// $total = $_SESSION['total_amount'];			//in online cart page it saved
	// $client_id = $_SESSION['client_id'];		//in online customer login page this session saved
	// $customer_id = $_SESSION['customer_id'];	//in online customer login page this session saved

	// $sql = "INSERT INTO online_orders (client_id, customer_id, total, order_date, order_status)
	// VALUES ('$client_id','$customer_id', $total,'$current_datetime','$order_status')";
	
	// //Order end
	// if (mysqli_query($conn, $sql) === TRUE) {
	// 	$last_order_id = $conn->insert_id;
	// 	// echo "New Order record created successfully";

	// 	$request = json_decode( file_get_contents('php://input'),true);
	// 	foreach ($request as $key => $items) {

	// 		$product_id = $items['product_id'];

	// 		$product_qty = $items['product_qty'];
	// 		$order_item_status = 0;
	// 		$sql = "INSERT INTO order_items (order_id, product_id, product_qty,	order_item_status)
	// 		VALUES ('$last_order_id', $product_id, $product_qty, order_item_status)";
	// 		if (mysqli_query($conn, $sql) === TRUE) {
	// 			echo "New Order_items record created successfully";
	// 		}
	// 	}
	// } else {
	// 	echo "Error: " . $sql . "<br>" . $conn->error;
	// }

	// payment gateway BEGIN

	// $firstname = $_SESSION['firstname'];	//Saved in customer info update.php
	// $email = $_SESSION['email'];	//in online customer login page this session saved
	//$phone = $_SESSION['phone']; //Saved in customer info update.php
	$firstname = 'sample';
	$email = 'brin@gmail.com';
	$phone  = 9698589262;

// Jess values
// $MERCHANT_KEY = "b6Ye9B";
// $SALT = "BBlZruym";
// $PAYU_BASE_URL = "https://secure.payu.in";


	// Merchant key here as provided by Payu
	$MERCHANT_KEY = "hDkYGPQe";

	// Merchant Salt as provided by Payu
	$SALT = "yIEkykqEH3";

	// End point - change to https://secure.payu.in for LIVE mode
	$PAYU_BASE_URL = "https://test.payu.in";

	$action = '';

	$posted = array();
	if(!empty($_POST)) {
    //print_r($_POST);
		foreach($_POST as $key => $value) {    
			$posted[$key] = $value; 
		}
	}

	// $formError = 0;

	if(empty($posted['txnid'])) 
	{
  // Generate random transaction id
		$txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
	} 
	else 
	{
		$txnid = $posted['txnid'];
	}
	$hash = '';
// Hash Sequence
	$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
	if(empty($posted['hash']) && sizeof($posted) > 0) {
		// if(empty($posted['key'])
		// 	|| empty($posted['txnid'])
		// 	|| empty($posted['amount'])
		// 	|| empty($posted['firstname'])
		// 	|| empty($posted['email'])
		// 	|| empty($posted['phone'])
		// 	|| empty($posted['productinfo'])
		// 	|| empty($posted['surl'])
		// 	|| empty($posted['furl'])
		// 	|| empty($posted['service_provider'])
		// 	) 
		// {
		// 	$formError = 1;
		// } 
		// else 
		// {

		$hashVarsSeq = explode('|', $hashSequence);
		$hash_string = ''; 
		foreach($hashVarsSeq as $hash_var) {
			$hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
			$hash_string .= '|';
		}

		$hash_string .= $SALT;

		$hash = strtolower(hash('sha512', $hash_string));
		$action = $PAYU_BASE_URL . '/_payment';
	}
	elseif(!empty($posted['hash'])) 
	{
		$hash = $posted['hash'];
		$action = $PAYU_BASE_URL . '/_payment';
	}

	$posted['surl'] = 'success.php';
	$posted['furl'] = 'failure.php';
	
	?>
	<html>
	<head>
		<script>
			var hash = '<?php echo $hash ?>';
			function submitPayuForm() {
				if(hash == '') {
					return;
				}
				var payuForm = document.forms.payuForm;
				payuForm.submit();
			}
		</script>
	</head>
	<body onload="submitPayuForm()">
		<h2>PayU Form</h2>
		<!-- <br/>
		<?php //if($formError) { ?>

		<span style="color:red">Please fill all mandatory fields.</span>
		<br/>
		<br/>
		<?php } ?> -->
		<form action="<?php echo $action; ?>" method="post" name="payuForm" id="payuFormID">
			<input type="hidden" name="key" value="<?php echo $MERCHANT_KEY ?>" />
			<input type="hidden" name="hash" value="<?php echo $hash ?>"/>
			<input type="hidden" name="txnid" value="<?php echo $txnid ?>" />
			<table>
				<tr>
					<td><b>Mandatory Parameters</b></td>
				</tr>
				<tr>
					<td>Amount: </td>
					<td><input name="amount" value="1" /></td>
					<td>First Name: </td>
					<td><input name="firstname" id="firstname" value="<?php echo $firstname; ?>" /></td>
				</tr>

				<tr>
					<td>Email: </td>
					<td><input name="email" id="email" value="<?php echo $email; ?>" /></td>
					<td>Phone: </td>
					<td><input name="phone" value="<?php echo $phone; ?>" /></td>
				</tr>
				
				<tr>
					<td>Product Info: </td>
					<td colspan="3"><textarea name="productinfo"><?php echo 'this is sample product' ?></textarea></td>
				</tr>

				<tr>
					<td>Success URI: </td>
					<td colspan="3"><input name="surl" value="<?php echo (empty($posted['surl'])) ? '' : $posted['surl'] ?>" size="64" /></td>
				</tr>
				<tr>
					<td>Failure URI: </td>
					<td colspan="3"><input name="furl" value="<?php echo (empty($posted['furl'])) ? '' : $posted['furl'] ?>" size="64" /></td>
				</tr>

				<tr>
					<td colspan="3"><input type="hidden" name="service_provider" value="payu_paisa" size="64" /></td>
				</tr>

				<tr>
					<td><b>Optional Parameters</b></td>
				</tr>
				<tr>
					<td>Last Name: </td>
					<td><input name="lastname" id="lastname" value="<?php echo (empty($posted['lastname'])) ? '' : $posted['lastname']; ?>" /></td>
					<td>Cancel URI: </td>
					<td><input name="curl" value="" /></td>
				</tr>
				<tr>
					<td>Address1: </td>
					<td><input name="address1" value="<?php echo (empty($posted['address1'])) ? '' : $posted['address1']; ?>" /></td>
					<td>Address2: </td>
					<td><input name="address2" value="<?php echo (empty($posted['address2'])) ? '' : $posted['address2']; ?>" /></td>
				</tr>
				<tr>
					<td>City: </td>
					<td><input name="city" value="<?php echo (empty($posted['city'])) ? '' : $posted['city']; ?>" /></td>
					<td>State: </td>
					<td><input name="state" value="<?php echo (empty($posted['state'])) ? '' : $posted['state']; ?>" /></td>
				</tr>
				<tr>
					<td>Country: </td>
					<td><input name="country" value="<?php echo (empty($posted['country'])) ? '' : $posted['country']; ?>" /></td>
					<td>Zipcode: </td>
					<td><input name="zipcode" value="<?php echo (empty($posted['zipcode'])) ? '' : $posted['zipcode']; ?>" /></td>
				</tr>
				<tr>
					<td>UDF1: </td>
					<td><input name="udf1" value="<?php echo (empty($posted['udf1'])) ? '' : $posted['udf1']; ?>" /></td>
					<td>UDF2: </td>
					<td><input name="udf2" value="<?php echo (empty($posted['udf2'])) ? '' : $posted['udf2']; ?>" /></td>
				</tr>
				<tr>
					<td>UDF3: </td>
					<td><input name="udf3" value="<?php echo (empty($posted['udf3'])) ? '' : $posted['udf3']; ?>" /></td>
					<td>UDF4: </td>
					<td><input name="udf4" value="<?php echo (empty($posted['udf4'])) ? '' : $posted['udf4']; ?>" /></td>
				</tr>
				<tr>
					<td>UDF5: </td>
					<td><input name="udf5" value="<?php echo (empty($posted['udf5'])) ? '' : $posted['udf5']; ?>" /></td>
					<td>PG: </td>
					<td><input name="pg" value="<?php echo (empty($posted['pg'])) ? '' : $posted['pg']; ?>" /></td>
				</tr>
				<tr>
					<?php if(!$hash) {
						?>

						<script type="text/javascript">
					    document.getElementById('payuFormID').submit(); // SUBMIT FORM
					</script>

					<td colspan="4"><input type="submit" value="Submit" /></td>
					<?php 
				}
				else
				{
  				    // leave the page alone
				}
				?>
			</tr>
		</table>
	</form>
</body>
</html>
<?
}
//paymentgateway END
}

?>
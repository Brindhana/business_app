<?php
session_start();
include('config.php');
	// Check connection
if (!$conn) {
	die("Connection failed: " . mysqli_connect_error());
}
else
{
	$request = json_decode( file_get_contents('php://input'),true);
	// {"product_id":"1","product_qty":4},{"product_id":"2","product_qty":7}
	$new_array = array();
	$_SESSION['total_amount'] = 0;
	foreach ($request as $key => $items) {
		$product_id = $items['product_id'];

		$product_qty = $items['product_qty'];

		$sql = "SELECT * FROM products WHERE product_id = $product_id";

		$result = mysqli_query($conn, $sql);

		$product_cart_item_arrays = array();

		$row = mysqli_fetch_assoc($result);
		
		$product_name = $row['product_name'];
		$product_price = $row['product_price'];

		$_SESSION['total_amount'] += $row['product_price'];

		$product_cart_item_arrays["product_id"] = $product_id;
		$product_cart_item_arrays["product_name"] = $product_name;
		$product_cart_item_arrays["product_price"] = $product_price;
		$product_cart_item_arrays["product_qty"] = $product_qty;
		
		array_push($new_array, $product_cart_item_arrays);
	}

	echo json_encode($new_array);
}
?>
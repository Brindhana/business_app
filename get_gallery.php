<?php
include('config.php');
	// Check connection
if (!$conn) {
	die("Connection failed: " . mysqli_connect_error());
}
else
{
	$request = json_decode( file_get_contents('php://input') );
	$client_id = $request->client_id;
	$path = $request->path;
	$page_url = explode("/",$path);
	$gallery_category_id = $page_url[3];
	$active_status = 1;    //Active Status

	$sql = "SELECT * FROM gallery AS g INNER JOIN gallery_category AS gc ON gc.gallery_category_id = g.gallery_category_id WHERE gc.client_id = $client_id && g.client_id = $client_id && g.gallery_category_id = $gallery_category_id && g.gallery_status = $active_status";

	$result = mysqli_query($conn, $sql);

	$gallery_arrays = array();
	while($row = mysqli_fetch_assoc($result)){	
		$gallery_arrays[] = $row;
	}

	echo json_encode($gallery_arrays);
}
?>